package com.newpage.polyschedule.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events")
data class PolyEventEntity (
    @PrimaryKey
    val id: Long,
    val title: String,
    val place: String,
    val teacher: String,
    val dateStart: Long,
    val dateEnd: Long,
    val month: Int,
    val year: Int
)