package com.newpage.polyschedule.database

import androidx.room.*

@Dao
interface PolyEventDao {
    @Query("SELECT * FROM events ORDER BY dateStart")
    suspend fun getAll(): List<PolyEventEntity>

    @Query("SELECT * FROM events WHERE month=:month AND year=:year ORDER BY dateStart")
    suspend fun getMonth(month: Int, year: Int): List<PolyEventEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(events: List<PolyEventEntity>)

    @Delete
    suspend fun delete(event: PolyEventEntity)

    @Query("DELETE FROM events")
    suspend fun clear()
}


@Database(entities = [PolyEventEntity::class], version = 1)
abstract class PolyEventsDatabase: RoomDatabase() {
    abstract fun polyEventsDao(): PolyEventDao
}