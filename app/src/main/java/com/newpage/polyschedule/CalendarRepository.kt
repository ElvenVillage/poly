package com.newpage.polyschedule

import android.os.Build
import androidx.annotation.RequiresApi
import com.newpage.polyschedule.database.PolyEventDao
import com.newpage.polyschedule.model.Group
import com.newpage.polyschedule.model.PolyEvent
import com.newpage.polyschedule.model.fromDomain
import com.newpage.polyschedule.model.toDomain
import com.newpage.polyschedule.util.parse
import com.newpage.polyschedule.util.parseGroups
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.*

class CalendarRepository(private val dao: PolyEventDao, private val client: OkHttpClient) {

    private fun generateRequestUrl(date: Calendar, groupCode: String): String {
        return "https://ruz.spbstu.ru/api/v1/ruz/scheduler/$groupCode?date=${date.get(Calendar.YEAR)}-${
            date.get(
                Calendar.MONTH
            ) + 1
        }-${date.get(Calendar.DAY_OF_MONTH)}"
    }

    suspend fun clear() {
        dao.clear()
    }

    suspend fun loadGroupName(query: String): Result<List<Group>> {
        return withContext(Dispatchers.IO) {
            val requestQuery = "https://ruz.spbstu.ru/search/groups?q=$query"
            val request = Request.Builder().url(requestQuery).build()

            try {
                val response = client.newCall(request).execute()
                return@withContext parseGroups(response.body.string())
            } catch (e: Exception) {
                return@withContext Result.failure(e)
            }


        }
    }

    suspend fun saveWithDao(events: List<PolyEvent>) {
        dao.insert(events.map { it.fromDomain() })
    }

    suspend fun loadFromDao(month: Int, year: Int): List<PolyEvent> {
        return dao.getMonth(month, year).map { it.toDomain() }
    }

    suspend fun loadFromNetwork(
        date: Calendar,
        groupCode: String = "36696"
    ): Result<List<PolyEvent>> {
        return withContext(Dispatchers.IO) {
            val daysToCheck = mutableListOf<Calendar>()

            val pointer = GregorianCalendar(date.get(Calendar.YEAR), date.get(Calendar.MONTH), 1)
            val month = date.get(Calendar.MONTH)

            //Moving forward
            do {
                daysToCheck.add(pointer.clone() as Calendar)
                pointer.add(Calendar.WEEK_OF_YEAR, 1)
            } while (pointer.get(Calendar.MONTH) == month)


            val result = mutableListOf<PolyEvent>()

            for (day in daysToCheck) {

                val request = Request.Builder()
                    .url(generateRequestUrl(day, groupCode))
                    .build()
                try {
                    val response = client.newCall(request).execute()
                    result.addAll(parse(response.body.string()) ?: listOf())
                } catch (e: Exception) {
                    return@withContext Result.failure(e)
                }
            }

            result.sortBy { it.dateStart.time }

            return@withContext Result.success(result)
        }
    }
}