package com.newpage.polyschedule.model

import com.newpage.polyschedule.database.PolyEventEntity
import java.util.*

data class PolyEvent(
    val id: Long,
    val title: String,
    val place: String,
    val teacher: String,
    val dateStart: Date,
    val dateEnd: Date,
)

data class Group (
    val groupCode: String, val groupName: String
)

fun PolyEventEntity.toDomain(): PolyEvent {
    return PolyEvent(
        id, title, place, teacher, Date(dateStart), Date(dateEnd)
    )
}

fun PolyEvent.fromDomain(): PolyEventEntity {
    return PolyEventEntity(
        id,
        title,
        place,
        teacher,
        dateStart.time,
        dateEnd.time,
        dateStart.month,
        dateStart.year + 1900
    );
}

