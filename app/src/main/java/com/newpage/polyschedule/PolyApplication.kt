package com.newpage.polyschedule

import android.app.Application
import com.newpage.polyschedule.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class PolyApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@PolyApplication)
            modules(appModule)
        }
    }
}