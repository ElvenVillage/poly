package com.newpage.polyschedule.ui


import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.newpage.polyschedule.R
import com.newpage.polyschedule.databinding.PolyEventBinding
import com.newpage.polyschedule.model.PolyEvent
import java.util.*
import kotlin.collections.HashMap


class PolyEventAdapter(private val events: List<PolyEvent>) :
    RecyclerView.Adapter<PolyEventAdapter.ViewHolder>() {
    private val expanded = HashMap<Int, Boolean>()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val timeTextView = PolyEventBinding.bind(view).polyEventTime
        val teacherTextView = PolyEventBinding.bind(view).polyEventTeacher
        val placeTextView = PolyEventBinding.bind(view).polyEventPlace
        val subjectTextView = PolyEventBinding.bind(view).polyEventTitle
        val root = PolyEventBinding.bind(view).polyEventRoot


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.poly_event, parent, false)

        return ViewHolder(view)

    }


    private fun formatDate(dateStart: Date, dateEnd: Date): String {
        val a = "${if (dateStart.hours < 10) "0" else ""}${dateStart.hours}"
        val b = "${if (dateStart.minutes < 10) "0" else ""}${dateStart.minutes}"
        val c = "${if (dateEnd.hours < 10) "0" else ""}${dateEnd.hours}"
        val d = "${if (dateEnd.minutes < 10) "0" else ""}${dateEnd.minutes}"
        return "${a}:${b} - ${c}:${d}"
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = events[position]
        if (expanded[position] == null) {
            expanded[position] = false
        }
        holder.apply {
            timeTextView.text = formatDate(event.dateStart, event.dateEnd)
            teacherTextView.text = event.teacher
            teacherTextView.visibility = if (event.teacher.isBlank()) GONE else VISIBLE
            placeTextView.text = event.place
            subjectTextView.text = event.title

        }

        holder.itemView.setOnClickListener {
            expanded[position] = expanded[position] != true
            holder.placeTextView.visibility = if (expanded[position] == true) VISIBLE else GONE
            TransitionManager.beginDelayedTransition(holder.root)

        }
    }

    override fun getItemCount() = events.size

}