package com.newpage.polyschedule.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.newpage.polyschedule.R
import com.newpage.polyschedule.databinding.GroupViewHolderBinding
import com.newpage.polyschedule.model.Group

class GroupAdapter(private val groups: List<Group>, val select: (group: Group) -> Unit) :
    RecyclerView.Adapter<GroupAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val groupName = GroupViewHolderBinding.bind(view).groupName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.group_view_holder, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.groupName.text = groups[position].groupName
        holder.groupName.setOnClickListener {
            select(groups[position])
        }
    }

    override fun getItemCount(): Int {
        return groups.size
    }
}