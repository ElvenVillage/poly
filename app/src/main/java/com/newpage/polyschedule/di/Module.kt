package com.newpage.polyschedule.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.newpage.polyschedule.CalendarRepository
import com.newpage.polyschedule.CalendarViewModel
import com.newpage.polyschedule.database.PolyEventDao
import com.newpage.polyschedule.database.PolyEventsDatabase
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { OkHttpClient.Builder().build() }

    fun provideDatabase(application: Application): PolyEventsDatabase {
        return Room.databaseBuilder(application, PolyEventsDatabase::class.java, "events").build()
    }

    fun provideDao(database: PolyEventsDatabase): PolyEventDao {
        return database.polyEventsDao()
    }

    fun provideSharedPrefs(app: Application): SharedPreferences {
        return app.applicationContext.getSharedPreferences(
            "groupName", Context.MODE_PRIVATE
        )
    }


    single { provideDatabase(androidApplication())  }

    single { provideSharedPrefs(androidApplication())  }

    single { provideDao(get()) }

    single { CalendarRepository(get(), get()) }

    viewModel { CalendarViewModel(get(), get()) }
}