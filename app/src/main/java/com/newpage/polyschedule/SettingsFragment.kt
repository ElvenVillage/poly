package com.newpage.polyschedule

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.newpage.polyschedule.databinding.FragmentCalendarBinding
import com.newpage.polyschedule.databinding.FragmentSettingsBinding
import com.newpage.polyschedule.model.Group
import com.newpage.polyschedule.ui.GroupAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*

class SettingsFragment : Fragment(R.layout.fragment_settings) {

    private val vm by sharedViewModel<CalendarViewModel>()

    private lateinit var adapter: GroupAdapter

    private var groups = mutableListOf<Group>()
    private var timer: Timer? = null

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (vm.searchedGroups.value?.isNotEmpty() == true) {
            groups.clear()
            groups.addAll(vm.searchedGroups.value!!)
        }
        adapter = GroupAdapter(groups) {
            vm.selectGroup(it)
            findNavController().popBackStack()
        }
        binding.groupList.adapter = adapter
        binding.groupList.layoutManager = LinearLayoutManager(context)

        vm.searchedGroups.observe(viewLifecycleOwner) {
            groups.clear()
            groups.addAll(it)
            adapter.notifyDataSetChanged()
        }

        binding.editTextGroupName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //nothing here
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                timer?.cancel()
            }

            override fun afterTextChanged(s: Editable?) {
                timer = Timer()
                timer!!.schedule(object : TimerTask() {
                    override fun run() {
                        vm.loadGroups(s.toString())
                    }
                }, 600)
            }

        })
    }
}
