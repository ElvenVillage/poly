package com.newpage.polyschedule

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.*
import com.newpage.polyschedule.model.Group
import com.newpage.polyschedule.model.PolyEvent
import kotlinx.coroutines.launch
import java.util.*

class CalendarViewModel(
    private val repository: CalendarRepository,
    private val prefs: SharedPreferences
) : ViewModel() {

    private val monthEvents = MutableLiveData<List<PolyEvent>>()
    val groupName = MutableLiveData<Group>()
    val dayEvents = MutableLiveData<List<PolyEvent>>(mutableListOf())

    private var selectedDay = GregorianCalendar()

    val noConnectError = MutableLiveData(false)
    val firstStart = MutableLiveData(false)
    val searchedGroups = MutableLiveData<List<Group>>(mutableListOf())

    init {
        loadGroupName()
        loadMonth(selectedDay)
    }

    fun loadGroups(query: String) {
        viewModelScope.launch {
            val result = repository.loadGroupName(query)
            if (result.isSuccess) {
                searchedGroups.value = result.getOrThrow()
            }
        }
    }

    fun selectGroup(group: Group) {
        prefs.edit {
            putString("code", group.groupCode)
            putString("name", group.groupName)
        }
        groupName.value = group
        firstStart.value = false
        loadMonth(null, deleteAll = true)
    }

    private fun loadGroupName() {
        val code = prefs.getString("code", null)
        val name = prefs.getString("name", null)

        if (code == null || name == null) {
            firstStart.value = true
        } else {
            groupName.value = Group(code!!, name!!)
        }
    }

    private fun filter(calendar: Calendar): List<PolyEvent> {
        return monthEvents.value!!.filter {
            it.dateStart.year + 1900 == calendar.get(Calendar.YEAR) && it.dateStart.month ==
                    calendar.get(Calendar.MONTH) && it.dateStart.date == calendar.get(Calendar.DAY_OF_MONTH)
        }
    }

    fun selectDay(year: Int, month: Int, day: Int) {
        val newSelection = GregorianCalendar(year, month, day)
        if (month != selectedDay.get(Calendar.MONTH)) {
            loadMonth(newSelection)
        } else {
            dayEvents.value = filter(newSelection)
        }
        selectedDay = newSelection
    }


    private fun loadMonth(currentDate: Calendar?, deleteAll: Boolean = false) {

        val date = currentDate ?: GregorianCalendar()
        if (groupName.value == null) return

        viewModelScope.launch {

            if (deleteAll) {
                repository.clear()
            }

            val dataFromDatabase = repository.loadFromDao(
                month = date.get(Calendar.MONTH),
                year = date.get(Calendar.YEAR)
            )
            if (dataFromDatabase.isNotEmpty()) {
                monthEvents.value = dataFromDatabase
                dayEvents.value = filter(date)
            }
            val dataFromNetwork =
                repository.loadFromNetwork(date, groupName.value!!.groupCode)

            if (dataFromNetwork.isFailure) {
                noConnectError.value = true
            }

            if (dataFromNetwork.isSuccess) {
                monthEvents.value = dataFromNetwork.getOrDefault(listOf())
                dayEvents.value = filter(date)
                repository.saveWithDao(dataFromNetwork.getOrDefault(listOf()))
            }


        }
    }
}