package com.newpage.polyschedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.newpage.polyschedule.databinding.FragmentCalendarBinding
import com.newpage.polyschedule.model.PolyEvent
import com.newpage.polyschedule.ui.PolyEventAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class CalendarFragment : Fragment(R.layout.fragment_calendar) {

    private val vm by sharedViewModel<CalendarViewModel>()

    private val events = mutableListOf<PolyEvent>()

    private var _binding: FragmentCalendarBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCalendarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.apply {
            setOnClickListener { goToSettingsScreen() }
            text = vm.groupName.value?.groupName
        }

        events.clear()

        val currentEvents = vm.dayEvents.value

        if (currentEvents?.isNotEmpty() == true)
            events.addAll(currentEvents)

        val adapter = PolyEventAdapter(
            events
        )
        binding.recyclerPolyEvents.apply {
            layoutManager = LinearLayoutManager(context)
        }
        binding.recyclerPolyEvents.adapter = adapter
        adapter.notifyDataSetChanged()

        vm.dayEvents.observe(viewLifecycleOwner) {
            events.clear()
            events.addAll(it)
            adapter.notifyDataSetChanged()
        }

        vm.groupName.observe(viewLifecycleOwner) {
            binding.button.text = vm.groupName.value?.groupName
        }

        vm.noConnectError.observe(viewLifecycleOwner) {
            if (it) {
                val toast = Toast.makeText(context, "Нет соединения", Toast.LENGTH_SHORT)
                toast.show()
                vm.noConnectError.value = false
            }
        }

        vm.firstStart.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(R.id.action_calendarFragment_to_settingsFragment)
            }
        }

        binding.calendar.setOnDateChangeListener { _, year, month, dayOfMonth ->
            vm.selectDay(year, month, dayOfMonth)

            adapter.notifyDataSetChanged()
        }
    }


    private fun goToSettingsScreen() {
        findNavController().navigate(R.id.action_calendarFragment_to_settingsFragment)
    }
}