package com.newpage.polyschedule.util

import android.os.Build
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.internal.LinkedTreeMap
import com.newpage.polyschedule.model.Group
import com.newpage.polyschedule.model.PolyEvent
import java.sql.Timestamp
import java.time.*
import java.util.*

fun parseGroups(body: String): Result<List<Group>> {

    val jsonString = body.substringAfter("__INITIAL_STATE__ =").substringBefore(";")
    print(jsonString)

    val result = mutableListOf<Group>()

    val gson = Gson()

    try {
        val dataMap = gson.fromJson(jsonString, Map::class.java)

        val groups =
            (dataMap["searchGroup"] as LinkedTreeMap<*, *>)["data"] as Iterable<LinkedTreeMap<*, *>>

        for (group in groups) {
            result.add(
                Group(
                    groupName = group["name"] as String,
                    groupCode = (group["id"] as Double).toInt().toString()
                )
            )
        }
        return Result.success(result)
    } catch (e: Exception) {
        return Result.failure(e)
    }
}

fun parse(rawResponse: String?): List<PolyEvent>? {
    if (rawResponse == null) return null
    val gson = Gson()

    try {
        val lessons = mutableListOf<PolyEvent>()
        val dataMap = gson.fromJson(rawResponse, Map::class.java)
        for (day in dataMap["days"] as Iterable<LinkedTreeMap<*, *>>) {
            val dayDate = LocalDate.parse(day["date"] as String)
            for (lesson in day["lessons"] as Iterable<LinkedTreeMap<*, *>>) {
                val startTime =
                    LocalDateTime.of(dayDate, LocalTime.parse(lesson["time_start"] as String))
                val endTime =
                    LocalDateTime.of(dayDate, LocalTime.parse(lesson["time_end"] as String))
                val title = lesson["subject"] as String
                val teacher =
                    if (lesson["teachers"] == null) "" else ((lesson["teachers"] as Iterable<LinkedTreeMap<*, *>>).first())["full_name"] as String
                val auditory = if (lesson["auditories"] == null) "" else
                    ((lesson["auditories"] as Iterable<LinkedTreeMap<*, *>>).first())["name"] as String

                val buildings = if (lesson["auditories"] == null) null else
                    ((lesson["auditories"] as Iterable<LinkedTreeMap<*, *>>).first())["building"] as LinkedTreeMap<*, *>
                val building =
                    if ((buildings?.get("name") as String).length > 30) buildings["abbr"] else buildings["name"]

                lessons.add(
                    PolyEvent(
                        id = Timestamp.from(startTime.toInstant(ZoneOffset.ofHours(0))).time,
                        teacher = teacher,
                        place = "$auditory $building",
                        title = title,
                        dateStart = Date.from(startTime.atZone(ZoneId.systemDefault()).toInstant()),
                        dateEnd = Date.from(endTime.atZone(ZoneId.systemDefault()).toInstant()),
                    )
                )
            }
        }


        return lessons
    } catch (e: JsonSyntaxException) {
        return null
    }

}